package struts.form;

import java.lang.reflect.Field;

import javax.servlet.http.HttpServletRequest;

public class FullForm {
	public static ActionForm  full(String formpath,HttpServletRequest request){
		 ActionForm form=null;
		 try {
			Class clazz=Class.forName(formpath);
			form=(ActionForm)clazz.newInstance();
			Field[]  field=clazz.getDeclaredFields();
			for(Field f:field){
				f.setAccessible(true);
				f.set(form,request.getParameter(f.getName()));
				f.setAccessible(false);
				
			}
			
			
			
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("严重");
		}
		 
		 
		 
		 
		 return form;
		
	}
	
	
	

}
