package servlet;

import java.util.Map;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import struts.form.StrutsParseXml;
import struts.form.XmlBean;

public class ActionListener implements ServletContextListener {

	public void contextDestroyed(ServletContextEvent arg0) {
	System.out.println("系统注销成功");

	}

	public void contextInitialized(ServletContextEvent arg0) {
		// TODO Auto-generated method stub
		String xmlPath=arg0.getServletContext().getInitParameter("struts-config");
		String tomcatPath=arg0.getServletContext().getRealPath("\\");
		try {
			Map<String ,XmlBean> map=StrutsParseXml.ParseXml(tomcatPath+xmlPath);
			arg0.getServletContext().setAttribute("struts",map);
			
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("系统已经加载完成");
		}
		

	}

}
