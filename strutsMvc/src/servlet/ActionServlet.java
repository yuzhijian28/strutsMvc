package servlet;

import java.io.IOException;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import struts.form.Action;
import struts.form.ActionForm;
import struts.form.FullForm;
import struts.form.XmlBean;

public class ActionServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
    public void doPost(HttpServletRequest request,HttpServletResponse response)throws IOException,ServletException{this.doGet(request,response);}    
	public void doGet(HttpServletRequest  request,HttpServletResponse response)throws IOException,ServletException{
		String path=this.getPath(request.getServletPath());
		Map<String,XmlBean>  map=(Map<String,XmlBean>)this.getServletContext().getAttribute("struts");
		XmlBean xml=map.get(path);
		String beanClass=xml.getBeanClass();
		System.out.println(xml.getActionForward());
		System.out.println(path+"======"+beanClass);
	    ActionForm  form=FullForm.full(beanClass, request);
		String actionType=xml.getActionType();
		Action action=null;
		String url="";
		try{
			Class clazz=Class.forName(actionType);
			action=(Action)clazz.newInstance();
			url=action.execute(request, response, form, xml.getActionForward());
			
		}catch(Exception e){
			e.printStackTrace();
			System.out.println("严重，控制器异常");
		}
		request.getRequestDispatcher(url).forward(request,response);
		
	}
	private String getPath(String servletPath){
		return servletPath.split("\\.")[0];
	}

	
	
	
	
	
	
}
